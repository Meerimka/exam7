import React, {Component} from 'react';
import './App.css';
import Menu from './components/menu';
import Order from './components/orderList';
import hamburgerImg from './assets/humburger.jpg';
import cheeseburgerImg from './assets/Cheeseburger.png';
import friesImg from './assets/fries.png';
import coffeeImg from './assets/coffee.jpeg';
import teaImg from './assets/tea.png';
import colaImg from './assets/cola.jpeg';

class App extends Component {
    state = {
        menu: [
            {name: 'Hamburger', count: 0, price: 80, image: hamburgerImg, currentPrice:0},
            {name: 'Cheeseburger', count: 0, price: 90, image: cheeseburgerImg,currentPrice:0},
            {name: 'Fries', count: 0, price: 45, image: friesImg, currentPrice:0 },
            {name: 'Coffee', count: 0, price: 30, image: coffeeImg, currentPrice:0},
            {name: 'Tea', count: 0, price: 20, image: teaImg, currentPrice:0},
            {name: 'Cola', count: 0, price: 35, image: colaImg,currentPrice:0}
        ],
        totalPrice: 0
    };
    addFood = item => {
        const menu = [...this.state.menu];
        for(let i=0; i<menu.length; i++){
            if(menu[i].name=== item.name){
                menu[i].count++;
                menu[i].currentPrice += menu[i].price ;
            }
        }
        let totalPrice = this.state.totalPrice;

        totalPrice += item.price;
        this.setState({menu, totalPrice});
    };
    removeFood = item => {
        const menu = [...this.state.menu];
        for(let i=0; i<menu.length; i++){
            if(menu[i].name=== item.name && menu[i].count !== 0 ){
                menu[i].count--;
                menu[i].currentPrice-=menu[i].price;
            }
        }
        let totalPrice = this.state.totalPrice;
            totalPrice -= item.price;

        this.setState({menu: menu, totalPrice});
    };

    totalPrice = () =>{
        const menu = [...this.state.menu];
        let totalPrice = 0;
        for(let i=0; i<menu.length; i++){
            totalPrice +=(menu[i].price * menu[i].count);
        }
        this.setState({totalPrice});
    };
  render() {
    return (
      <div className="App">
        <div className="container">
            <div className="menu-container"><span className="Items">Add Items</span>
          {this.state.menu.map((menu, index)=>{
              return (
                  <Menu menu={menu}
                        addFood={this.addFood}
                  />
              )
          })}
          </div>
          <Order
              total = {this.state.totalPrice}
              menu={this.state.menu}
              remove={(name) => this.removeFood(name)}
          />
        </div>
      </div>
    );
  }
}

export default App;
