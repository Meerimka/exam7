import React ,{Fragment} from 'react';
import './menu.css';
import Single from'../components/onefood';


const Order = props =>
    <Fragment>
        <div className="Order"><span className="Items">Order details</span>
            <div className="Price">Total price: {props.total} som </div>
            {props.menu.map((name,index)=>{
                return(
                    <Single
                        key={index} name={name.name} count={name.count}
                    >
                        <span>{" "+ name.currentPrice}KGS</span>
                        {props.count !== 0 ? <button className="remove" onClick ={()=> props.remove(name)}>x</button> : null}
                    </Single>
                )
            })
            }

        </div>
    </Fragment>;

export default Order;
