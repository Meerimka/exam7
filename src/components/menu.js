import React from 'react';
import './menu.css';


const  Menu = props => (
    <div className="Menu">
        <button onClick={()=> props.addFood(props.menu)}>
            <img className="Images" src={props.menu.image} />
        </button>
        <span>{props.menu.name+" "}</span>
        <span>{props.menu.price+" "}KGS</span>
    </div>
);

export default Menu;

