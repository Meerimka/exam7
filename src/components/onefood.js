import React, {Fragment} from 'react';

const Single = (props)=>{
    const food = ()=> {
        let oneFood = [];
        for (let i = 0; i < props.count; i++) {
            oneFood.splice(0,1, props.name);
        }
        return oneFood;
    };
    return (
        food().map((menu, index)=>{
            return(
                <Fragment>
                <div className="item-box" key={index}>{props.name+ " "}
                    <span>{" " + props.count}x</span>
                    <span>{props.children}</span>
                </div>

                </Fragment>

            )
        })
    )


};
export default Single;